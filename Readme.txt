B-Spline Re-sampling, developed in CVC, UT-Austin.
This program generates a re-sampled volume from a portion of the original volume.
Usage:
./cvc-spline Inputfile Outputfile SplineOrder startX startY startZ SubVolumeSizeX SubVolumeSizeY SubVolumeSizeZ newVolumeSizeX newVolumeSizeY newVolumeSizeZ
where:
        Inputfile - a file in RAWIV format
        Outputfile - a file in RAWIV format
        SplineOrder - an integer to specify the B-spline order, must be between 1-5
                1: linear B-spline
                2: qudratic B-spline
                3: cubic B-spline
                4: quartic B-spline
                5: quintic B-spline
        startX - the subvolume starting location from the origin along X in the input volume
        startY - the subvolume starting location from the origin along Y in the input volume
        startZ - the subvolume starting location from the origin along Z in the input volume
        SubVolumeSizeX - the subvolume size along X, will be reduced if larger than allowed
        SubVolumeSizeY - the subvolume size along Y, will be reduced if larger than allowed
        SubVolumeSizeZ - the subvolume size along Z, will be reduced if larger than allowed
        newVolumeSizeX - the resulting volume size along X
        newVolumeSizeY - the resulting volume size along Y
        newVolumeSizeZ - the resulting volume size along Z

