#ifdef WIN32
#include <windows.h>
#endif


#include "rawivutil.h"   
#include "spline.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

int string_ends_with(const char * str, const char * suffix)
{
    int str_len = strlen(str);
    int suffix_len = strlen(suffix);

    return
        (str_len >= suffix_len) &&
        (0 == strcmp(str + (str_len - suffix_len), suffix));
}

//using namespace std;

void usage(char *appname)
{
	printf("B-Spline Re-sampling, developed in CVC, UT-Austin.\n");
	printf("This program generates a re-sampled volume from a portion of the original volume.\n");
	printf("Usage:\n");
	printf("%s Inputfile Outputfile SplineOrder startX startY startZ SubVolumeSizeX SubVolumeSizeY SubVolumeSizeZ newVolumeSizeX newVolumeSizeY newVolumeSizeZ\n", appname);
	printf("where:\n");
	printf("\tInputfile - a file in RAWIV format\n");
	printf("\tOutputfile - a file in RAWIV format\n");
	printf("\tSplineOrder - an integer to specify the B-spline order, must be between 1-5\n");
	printf("\t\t1: linear B-spline\n");
	printf("\t\t2: qudratic B-spline\n");
	printf("\t\t3: cubic B-spline\n");
	printf("\t\t4: quartic B-spline\n");
	printf("\t\t5: quintic B-spline\n");
	printf("\tstartX - the subvolume starting location from the origin along X in the input volume\n");
	printf("\tstartY - the subvolume starting location from the origin along Y in the input volume\n");
	printf("\tstartZ - the subvolume starting location from the origin along Z in the input volume\n");
	printf("\tSubVolumeSizeX - the subvolume size along X, will be reduced if larger than allowed\n");
	printf("\tSubVolumeSizeY - the subvolume size along Y, will be reduced if larger than allowed\n");
	printf("\tSubVolumeSizeZ - the subvolume size along Z, will be reduced if larger than allowed\n");
	printf("\tnewVolumeSizeX - the resulting volume size along X\n");
	printf("\tnewVolumeSizeY - the resulting volume size along Y\n");
	printf("\tnewVolumeSizeZ - the resulting volume size along Z\n");
}

void reorderchar(char* input)
{
    char tempchar;
	tempchar = input[0];
	input[0] = input[3];
	input[3] = tempchar;
	tempchar = input[1];
	input[1] = input[2];
	input[2] = tempchar;
}

void process_args(int argc, char* argv[]) 
{
    int ordernum = atoi(argv[3]);

    int startLocation[3];
    startLocation[0] = (int)atoi(argv[4]);
    startLocation[1] = (int)atoi(argv[5]);
    startLocation[2] = (int)atoi(argv[6]);

    int subSize[3];
    subSize[0] = (int)atoi(argv[7]);
    subSize[1] = (int)atoi(argv[8]);
    subSize[2] = (int)atoi(argv[9]);

    int newDim[3];
    newDim[0] = (int)(atoi(argv[10]));
    newDim[1] = (int)(atoi(argv[11]));
    newDim[2] = (int)(atoi(argv[12]));
    BspineResample(argv[1], argv[2], ordernum, startLocation, subSize, newDim);
    return;

}

int main(int argc, char *argv[])
{
	if(argc < 13) {
		usage(argv[0]);
		return -1;
	}
    process_args(argc, argv);

    return 0;
}

void BspineResample(char* inFName, char* outFName, int ordernum, int* startLocation, 
					int *subSize, int* newDim)
{
	struct rawiv inputVolume = {0};
	struct rawiv outputVolume = {0};
	struct rawiv subVolume = {0};
	int i, j, k;
	float f[3];
   
	clock_t start, finish;
	start = clock();
    if (string_ends_with(inFName, ".rawiv"))
        readRawiv(inFName, inputVolume);
    else
        readRawivFromImage(inFName, inputVolume);

	rawivDataToFloat(inputVolume);

	assert(startLocation[0] >= 0 && startLocation[1] >= 0 && startLocation[2] >= 0);
	assert(subSize[0] > 0 && subSize[1] > 0 && subSize[2] > 0);
	assert(newDim[0] > 0 && newDim[1] > 0 && newDim[2] >0);
	
	for(i = 0; i < 3; i++)
	{
		if((subSize[i] + startLocation[i]) > (int)inputVolume.dim[i]) {
			subSize[i] = inputVolume.dim[i] - startLocation[i];
			if(i == 0)
				printf("The subVolume Size along X is set too large, reduced to ");
			else if (i == 1)
				printf("The subVolume Size along Y is set too large, reduced to ");
			else
				printf("The subVolume Size along Z is set too large, reduced to ");
			printf("%d\n", subSize[i]);
		}
	}
	assert(subSize[0] > 0 && subSize[1] > 0 && subSize[2] > 0);
	setRawivDim(subVolume, subSize);

	for(i = 0; i < 3; i++) {
        if (inputVolume.dim[i] > 1)
            f[i] = (inputVolume.maxext[i] - inputVolume.minext[i]) / (inputVolume.dim[i] - 1);
        else
            f[i] = 0;
	}
	setRawivSpan(subVolume, f);

	for(i = 0; i < 3; i++) {
		f[i] = inputVolume.minext[i] + subVolume.span[i]*startLocation[i];
	}
	setRawivMinExt(subVolume, f);

	for(i = 0; i < 3; i++) {
		f[i] = subVolume.minext[i] + subVolume.span[i]*(subSize[i]-1);
	}
	setRawivMaxExt(subVolume, f);

	setRawivOrig(subVolume, inputVolume.orig);
	
	float* tempData = (float*)malloc(sizeof(float)*subVolume.nverts);
	for(k = 0; k < subSize[2]; k++) {
		for(j = 0; j < subSize[1]; j++) {
			for(i = 0; i < subSize[0]; i++) {
				tempData[k*subSize[0]*subSize[1]+ j*subSize[0] +i] =
					inputVolume.fDataset[(k+startLocation[2])*inputVolume.dim[1]*inputVolume.dim[0] 
						+ (j+startLocation[1])*inputVolume.dim[0] + (i+startLocation[0])];
			}
		}
	}

	setRawivData(subVolume, tempData);
	free(tempData);
	freeRawiv(inputVolume);

	
	printf("the selected subvolume is %d x %d x %d starting at[%d, %d, %d]\n", subVolume.dim[0], subVolume.dim[1], subVolume.dim[2],
		startLocation[0], startLocation[1], startLocation[2]);
	printf("the original interval along three directions are %f %f %f\n", subVolume.span[0], subVolume.span[1], subVolume.span[2]);


   

	printf("you choose the spline order as %d \n",ordernum);

	printf("and the new volume resolutions are %d %d %d\n", newDim[0], newDim[1], newDim[2]);

	float *NewImg=NULL;
	float *SplineCoeff=NULL;
	float *NewSplineCoeff=NULL;
	float *PostSplineCoeff=NULL;

	
	if(NewImg!=NULL){ 
		free(NewImg); 
		NewImg=NULL;
	}

	NewImg=(float *)calloc(newDim[0]*newDim[1]*newDim[2],sizeof(float));

	if(SplineCoeff!=NULL){ 
		free(SplineCoeff); 
		SplineCoeff=NULL;
	}

	SplineCoeff=(float *)calloc(subVolume.nverts,sizeof(float));


	if(NewSplineCoeff!=NULL){ 
		free(NewSplineCoeff); 
		NewSplineCoeff=NULL;
	}

	NewSplineCoeff=(float *)calloc(newDim[0]*newDim[1]*newDim[2],sizeof(float));


	if(PostSplineCoeff!=NULL){ 
		free(PostSplineCoeff); 
		PostSplineCoeff=NULL;
	}

	PostSplineCoeff=(float *)calloc(newDim[0]*newDim[1]*newDim[2],sizeof(float));
 


////////////////////////////////////////////////////////////////

	TransImg2Spline(subVolume.fDataset,SplineCoeff,subVolume.dim[0],subVolume.dim[1],subVolume.dim[2],ordernum);

	Resample(SplineCoeff,NewSplineCoeff,subVolume.dim[0],subVolume.dim[1],subVolume.dim[2],newDim[0],newDim[1],newDim[2],ordernum);
	PostFiltering(NewSplineCoeff,NewImg,newDim[0],newDim[1],newDim[2],ordernum);

//////////////////////////////////////////////////////////////

	setRawivDim(outputVolume, newDim);
	setRawivMinExt(outputVolume, subVolume.minext);
	setRawivMaxExt(outputVolume, subVolume.maxext);
	setRawivOrig(outputVolume, subVolume.orig);

	f[0] = (outputVolume.maxext[0] - outputVolume.minext[0]) / (outputVolume.dim[0] - 1);
	f[1] = (outputVolume.maxext[1] - outputVolume.minext[1]) / (outputVolume.dim[1] - 1);
	f[2] = (outputVolume.maxext[2] - outputVolume.minext[2]) / (outputVolume.dim[2] - 1);
	setRawivSpan(outputVolume, f);

	setRawivData(outputVolume, NewImg);

	if(NewImg!=NULL)  { 
		free(NewImg); 
		NewImg=NULL;
	}
	if(SplineCoeff!=NULL) {
		free(SplineCoeff); 
		SplineCoeff=NULL;
	}
	if(NewSplineCoeff!=NULL) {
		free(NewSplineCoeff); 
		NewSplineCoeff=NULL;
	}
	if(PostSplineCoeff!=NULL) {
		free(PostSplineCoeff); 
		PostSplineCoeff=NULL;
	}

    if (string_ends_with(outFName, ".rawiv"))
        writeRawiv(outFName, outputVolume);
    else
        writeRawivToImage(outFName, outputVolume);

	freeRawiv(subVolume);
	freeRawiv(outputVolume);
	finish = clock();

	double  duration = (double)(finish - start) / CLOCKS_PER_SEC;
	printf("DONE!\n");
	printf("The total running time is: %.3f seconds\n", duration);
	return;
}












