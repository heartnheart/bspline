/*************************************************************
This file provides an interface to manipulate rawiv files, as well as to
compute the root mean squared difference of two rawiv files, which are 
of the same dimensions and data type.
*************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <assert.h>
#include <math.h>
#include <algorithm>
#include "rawivutil.h"
#include "stb_image.h"
#include "stb_image_write.h"
#include <iostream>
#define ALL(x) begin(x), end(x)

#define IndexVect(i,j,k) ((k)*XDIM*YDIM + (j)*XDIM + (i))

void reverseAFloat(float* in)
{
	unsigned char *chartempf;
	unsigned char chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[0];
	chartempf[0] = chartempf[3];
	chartempf[3] = chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[1];
	chartempf[1] = chartempf[2];
	chartempf[2] = chartemp;
}

void reverseAnInt(int* in)
{
	unsigned char *chartempf;
	unsigned char chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[0];
	chartempf[0] = chartempf[3];
	chartempf[3] = chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[1];
	chartempf[1] = chartempf[2];
	chartempf[2] = chartemp;
}

void reverseAnUnInt(unsigned int* in)
{
	unsigned char *chartempf;
	unsigned char chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[0];
	chartempf[0] = chartempf[3];
	chartempf[3] = chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[1];
	chartempf[1] = chartempf[2];
	chartempf[2] = chartemp;
}

void reverseAShort(short* in)
{
	unsigned char *chartempf;
	unsigned char chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[0];
	chartempf[0] = chartempf[1];
	chartempf[1] = chartemp;
}

void reverseAnUnShort(unsigned short* in)
{
	unsigned char *chartempf;
	unsigned char chartemp;
	chartempf = (unsigned char *)in;
	chartemp = chartempf[0];
	chartempf[0] = chartempf[1];
	chartempf[1] = chartemp;
}

void readRawivHeader(FILE* fp, float* minext, float* maxext, int& nverts, int& ncells, unsigned int* dim, float* orig, float* span)
{
	/* reading RAWIV header */
	fread(minext, sizeof(float), 3, fp);
	fread(maxext, sizeof(float), 3, fp);
	fread(&nverts, sizeof(int), 1, fp);
	fread(&ncells, sizeof(int), 1, fp);
	fread(dim, sizeof(unsigned int), 3, fp);
	fread(orig, sizeof(float), 3, fp);
	fread(span, sizeof(float), 3, fp);

	int temp = 1;
	unsigned char* chartempf = (unsigned char*) &temp;
	int i;
	if(chartempf[0] > '\0') 
	{
		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(minext[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(maxext[i]));
		}

		reverseAnInt(&nverts);
		reverseAnInt(&ncells);

		for(i = 0; i < 3; i++)
		{
			reverseAnUnInt(&(dim[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(orig[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(span[i]));
		}
	}
	assert(nverts > 0);
	assert(dim[0] > 0);
	assert(dim[1] > 0);
	assert(dim[2] > 0);
}

void writeRawivHeader(FILE* fp, float* minext, float* maxext, int& nverts, int& ncells, unsigned int* dim, float* orig, float* span)
{
	int temp = 1;
	int i;
	unsigned char* chartempf = (unsigned char*) &temp;
	if(chartempf[0] > '\0') 
	{
		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(minext[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(maxext[i]));
		}

		reverseAnInt(&nverts);
		reverseAnInt(&ncells);

		for(i = 0; i < 3; i++)
		{
			reverseAnUnInt(&(dim[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(orig[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(span[i]));
		}
	}
	/* writing RAWIV header */
	fwrite(minext, sizeof(float), 3, fp);
	fwrite(maxext, sizeof(float), 3, fp);
	fwrite(&nverts, sizeof(int), 1, fp);
	fwrite(&ncells, sizeof(int), 1, fp);
	fwrite(dim, sizeof(unsigned int), 3, fp);
	fwrite(orig, sizeof(float), 3, fp);
	fwrite(span, sizeof(float), 3, fp);
	// restore original values
	if(chartempf[0] > '\0') 
	{
		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(minext[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(maxext[i]));
		}

		reverseAnInt(&nverts);
		reverseAnInt(&ncells);

		for(i = 0; i < 3; i++)
		{
			reverseAnUnInt(&(dim[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(orig[i]));
		}

		for(i = 0; i < 3; i++)
		{
			reverseAFloat(&(span[i]));
		}
	}
}

void readRawiv(char* filename, struct rawiv& r)
{
	FILE* fp;
	struct stat filestat;
	if ((fp=fopen(filename, "rb"))==NULL){
		printf("read error...\n");
		exit(0);
	}
	stat(filename, &filestat);
	readRawivHeader(fp, r.minext, r.maxext, r.nverts, r.ncells, r.dim, r.orig, r.span);	  
	size_t size[3];
	size[0] = 12 * sizeof(float) + 2 * sizeof(int) + 3 * sizeof(unsigned int) +
			r.dim[0] * r.dim[1] * r.dim[2] * sizeof(unsigned char);
	size[1] = 12 * sizeof(float) + 2 * sizeof(int) + 3 * sizeof(unsigned int) +
			r.dim[0] * r.dim[1] * r.dim[2] * sizeof(unsigned short);
	size[2] = 12 * sizeof(float) + 2 * sizeof(int) + 3 * sizeof(unsigned int) +
			r.dim[0] * r.dim[1] * r.dim[2] * sizeof(float);

	int found, datatype;
	unsigned int i, j, k;
	found = 0;
	for (i = 0; i < 3; i++)
		if (size[i] == (size_t) filestat.st_size)
		{
			if (found == 0)
			{
				datatype = i;
				found = 1;
			}
		}
	if (found == 0)
	{
		printf("Corrupted file or unsupported dataset type\n");
		exit(-1);
	}

	r.ucDataset = NULL;
	r.usDataset = NULL;
	r.fDataset = NULL;

	if (datatype == 0) {
		printf("file %s has unsigned char data\n\n", filename);
		r.ucDataset = (unsigned char*) malloc(sizeof(unsigned char) * r.dim[0] * r.dim[1] * r.dim[2]);
		unsigned char c_unchar;
		for (i=0; i<r.dim[2]; i++) {
			for (j=0; j<r.dim[1]; j++)
				for (k=0; k<r.dim[0]; k++) {
					fread(&c_unchar, sizeof(unsigned char), 1, fp);
					r.ucDataset[i*r.dim[0]*r.dim[1]+j*r.dim[0]+k] = c_unchar;
				}
		}
	}

	else if (datatype == 1) {
		printf("file %s has unsigned short data\n\n", filename);
		r.usDataset = (unsigned short*) malloc(sizeof(unsigned short) * r.dim[0] * r.dim[1] * r.dim[2]);
		unsigned short c_unshort;
		int temp;
		unsigned char* chartempf;
		for (i=0; i<r.dim[2]; i++) {
			for (j=0; j<r.dim[1]; j++) {
				for (k=0; k<r.dim[0]; k++) {
					fread(&c_unshort, sizeof(unsigned short), 1, fp);
					temp = 1;
					chartempf = (unsigned char*) &temp;
					if(chartempf[0] > '\0') {
						reverseAnUnShort(&c_unshort);
					}
					r.usDataset[i*r.dim[0]*r.dim[1]+j*r.dim[0]+k] = c_unshort;;
				}
			}
		}
	}

	else if (datatype == 2) {
		printf("file %s has float data\n\n", filename);
		r.fDataset = (float*) malloc(sizeof(float) * r.dim[0] * r.dim[1] * r.dim[2]);
		float c_float;
		int temp;
		unsigned char* chartempf;
		for (i=0; i<r.dim[2]; i++) {
			for (j=0; j<r.dim[1]; j++) {
				for (k=0; k<r.dim[0]; k++) {
					fread(&c_float, sizeof(float), 1, fp);
					temp = 1;
					chartempf = (unsigned char*) &temp;
					if(chartempf[0] > '\0') 
						reverseAFloat(&c_float);
					r.fDataset[i*r.dim[0]*r.dim[1]+j*r.dim[0]+k] = c_float;
				}
			}
		}
	}

	else {
		printf("error\n");
		fclose(fp);
		exit(1);
	}

	fclose(fp);
}

void readRawivFromImage(const char* filename, struct rawiv& r)
{
    using namespace std;
    int x, y, n;
    unsigned char *data = stbi_load(filename, &x, &y, &n, 1);

    //set rawiv header
    fill(ALL(r.minext), 0);
    r.maxext[0] = x - 1;
    r.maxext[1] = y - 1;
    r.maxext[2] = 0;
    r.nverts = x * y;
    r.ncells = (x - 1) * (y - 1);
    r.dim[0] = x;
    r.dim[1] = y;
    r.dim[2] = 1;
    fill(ALL(r.orig), 0);
    r.span[0] = (r.maxext[0] - r.minext[0]) / (float)x;
    r.span[1] = (r.maxext[1] - r.minext[1]) / (float)y;
    r.span[2] = 0;


    r.ucDataset = data;
}

void writeRawivToImage(const char* filename, struct rawiv& r)
{
    rawivDatatoUChar(r);
    stbi_write_png(filename, r.dim[0], r.dim[1], 1, r.ucDataset, 0);
}

void writeRawiv(char* filename, struct rawiv& r) 
{
	FILE* fp;
	if ((fp=fopen(filename, "wb")) == NULL){
		printf("write error....\n");
		exit(-1);
	}

	writeRawivHeader(fp, r.minext, r.maxext, r.nverts, r.ncells, r.dim, r.orig, r.span);

	unsigned int i;
	int temp;
	unsigned char* chartempf;

	if(r.ucDataset) {
		fwrite(r.ucDataset, sizeof(unsigned char), r.dim[0]*r.dim[1]*r.dim[2], fp);
	}
	else if(r.usDataset) {
		temp = 1;
		chartempf = (unsigned char*) &temp;
		if(chartempf[0] > '\0') {
			for (i=0; i<r.dim[0]*r.dim[1]*r.dim[2]; i++) {
				reverseAnUnShort(&(r.usDataset[i]));
			}
		}
		fwrite(r.usDataset, sizeof(unsigned short), r.dim[0]*r.dim[1]*r.dim[2], fp);
	}
	else {
		temp = 1;
		chartempf = (unsigned char*) &temp;
		if(chartempf[0] > '\0') {
			for (i=0; i<r.dim[0]*r.dim[1]*r.dim[2]; i++) {
				reverseAFloat(&(r.fDataset[i]));
			}
		}
		fwrite(r.fDataset, sizeof(float), r.dim[0]*r.dim[1]*r.dim[2], fp);
		//restore original values
		if(chartempf[0] > '\0') {
			for (i=0; i<r.dim[0]*r.dim[1]*r.dim[2]; i++) {
				reverseAFloat(&(r.fDataset[i]));
			}
		}
	}

	fclose(fp);
}

void freeRawiv(struct rawiv& r)
{
	if(r.ucDataset) {
		free(r.ucDataset);
		r.ucDataset = NULL;
	}
	if(r.usDataset) {
		free(r.usDataset);
		r.usDataset = NULL;
	}
	if(r.fDataset) {
		free(r.fDataset);
		r.fDataset = NULL;
	}
}

void rawivDataToFloat(struct rawiv& r)
{
	if(r.fDataset)
		return;
	r.fDataset = (float *)malloc(sizeof(float) * r.dim[0] * r.dim[1] * r.dim[2]);
	if(r.ucDataset) {
		for(unsigned int i = 0; i < r.dim[0] * r.dim[1] * r.dim[2]; i++)
			r.fDataset[i] = (float) r.ucDataset[i];
	}
	if(r.usDataset) {
		for(unsigned int i = 0; i < r.dim[0] * r.dim[1] * r.dim[2]; i++)
			r.fDataset[i] = (float) r.usDataset[i];
	}
    
}

void rawivDatatoUChar(struct rawiv& r)
{
    if (r.ucDataset)
        return;
    const int VERTS_CNT = r.dim[0] * r.dim[1] * r.dim[2];
    r.ucDataset = (unsigned char*)malloc(sizeof(unsigned char) * VERTS_CNT);

    if(r.usDataset) {
        for (int i = 0; i < VERTS_CNT; ++i)
            r.ucDataset[i] = (unsigned char)r.usDataset[i];
    }
    if (r.fDataset) {
        auto pr = std::minmax_element(r.fDataset, r.fDataset + VERTS_CNT);
        std::cout << "Float: min=" << *pr.first << ", max=" << *pr.second << "\n";

        for (int i = 0; i < VERTS_CNT; ++i)
            if (r.fDataset[i] >= 255)
                r.ucDataset[i] = 255;
            else if (r.fDataset[i] <= 0)
                r.ucDataset[i] = 0;
            else
                r.ucDataset[i] = (unsigned char)r.fDataset[i];
        auto pr2 = std::minmax_element(r.ucDataset, r.ucDataset + VERTS_CNT);
        std::cout << "Convert to char: min=" << (int)*pr2.first << ", max=" << (int)*pr2.second << "\n";
    }

}

void setRawivMinExt(struct rawiv& r, float f[3])
{
	r.minext[0] = f[0];
	r.minext[1] = f[1];
	r.minext[2] = f[2];
}

void setRawivMaxExt(struct rawiv& r, float f[3])
{
	r.maxext[0] = f[0];
	r.maxext[1] = f[1];
	r.maxext[2] = f[2];
}

void setRawivDim(struct rawiv& r, int i[3])
{
	r.dim[0] = i[0];
	r.dim[1] = i[1];
	r.dim[2] = i[2];
	r.nverts = r.dim[0] * r.dim[1] * r.dim[2];
	r.ncells = (r.dim[0]-1) * (r.dim[1]-1) * (r.dim[2]-1);
}

void setRawivOrig(struct rawiv& r, float f[3])
{
	r.orig[0] = f[0];
	r.orig[1] = f[1];
	r.orig[2] = f[2];
}

void setRawivSpan(struct rawiv& r, float f[3])
{
	r.span[0] = f[0];
	r.span[1] = f[1];
	r.span[2] = f[2];
}

void setRawivData(struct rawiv& r, unsigned char* d)
{
	int i;
	if(r.ucDataset)
		free(r.ucDataset);
	r.ucDataset = (unsigned char*) malloc(sizeof(unsigned char) * r.nverts);
	for(i = 0; i < r.nverts; i++)
		r.ucDataset[i] = d[i];
}

void setRawivData(struct rawiv& r, unsigned short* d)
{
	int i;
	if(r.usDataset)
		free(r.usDataset);
	r.usDataset = (unsigned short*) malloc(sizeof(unsigned short) * r.nverts);
	for(i = 0; i < r.nverts; i++)
		r.usDataset[i] = d[i];
}

void setRawivData(struct rawiv& r, float* d)
{
	int i;
	if(r.fDataset)
		free(r.fDataset);
	r.fDataset = (float*) malloc(sizeof(float) * r.nverts);
	for(i = 0; i < r.nverts; i++)
		r.fDataset[i] = d[i];
}

