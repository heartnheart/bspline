#include "spline.h"


#define CVC_DBL_EPSILON 1.0e-9f



/*--------------------------------------------------------------------------*/
float  InitialAntiCausalCoefficient
		(
			float	*c,		/* coefficients */
			int	DataLength,	/* number of samples or coefficients */
			float	z			/* actual pole */
		)

{ /* begin InitialAntiCausalCoefficient */

	/* this initialization corresponds to mirror boundaries */
	return ((z / (z * z - 1.0f)) * (z * c[DataLength - 2] + c[DataLength - 1]));
} /* end InitialAntiCausalCoefficient */







/*--------------------------------------------------------------------------*/
float	InitialCausalCoefficient
		(
			float	*c,		/* coefficients */
			int	DataLength,	/* number of coefficients */
			float	z,			/* actual pole */
			float	Tolerance	/* admissible relative error */
		)

{ /* begin InitialCausalCoefficient */

	float	Sum, zn, z2n, iz;
	int	n, Horizon;

	/* this initialization corresponds to mirror boundaries */
	Horizon = DataLength;
	if (Tolerance > 0.0) {
		Horizon = (int)ceil(log(Tolerance) / log(fabs(z)));
	}
	if (Horizon < DataLength) {
		/* accelerated loop */
		zn = z;
		Sum = c[0];
		for (n = 1; n < Horizon; n++) {
			Sum += zn * c[n];
			zn *= z;
		}
		return(Sum);
	}
	else {
		/* full loop */
		zn = z;
		iz = 1.0f / z;
		z2n = (float)pow(z, (DataLength - 1));
		Sum = c[0] + z2n * c[DataLength - 1];
		z2n *= z2n * iz;
		for (n = 1; n <= DataLength - 2; n++) {
			Sum += (zn + z2n) * c[n];
			zn *= z;
			z2n *= iz;
		}
		return(Sum / (1.0f - zn * zn));
	}
} /* end InitialCausalCoefficient */





/*--------------------------------------------------------------------------*/
 void	ConvertToInterpolationCoefficients
		(
			float	*c,		/* input samples --> output coefficients */
			int	DataLength,	/* number of samples or coefficients */
			float	*z,		/* poles */
			int	NbPoles,	/* number of poles */
			float	Tolerance	/* admissible relative error */
		)

{ /* begin ConvertToInterpolationCoefficients */

	float	Lambda = 1.0;
	int	n, k;

	/* special case required by mirror boundaries */
	if (DataLength == 1) {
		return;
	}
	/* compute the overall gain */
	for (k = 0; k < NbPoles; k++) {
		Lambda = Lambda * (1.0f - z[k]) * (1.0f - 1.0f / z[k]);
	}
	/* apply the gain */
	for (n = 0; n < DataLength; n++) {
		c[n] *= Lambda;
	}
	/* loop over all poles */
	for (k = 0; k < NbPoles; k++) {
		/* causal initialization */
		c[0] = InitialCausalCoefficient(c, DataLength, z[k], Tolerance);
		/* causal recursion */
		for (n = 1; n < DataLength; n++) {
			c[n] += z[k] * c[n - 1L];
		}
		/* anticausal initialization */
		c[DataLength - 1] = InitialAntiCausalCoefficient(c, DataLength, z[k]);
		/* anticausal recursion */
		for (n = DataLength - 2; 0 <= n; n--) {
			c[n] = z[k] * (c[n + 1] - c[n]);
		}
	}
} /* end ConvertToInterpolationCoefficients */





//--------------------------------------//

//--------------------------------------//


void	TransImg2Spline
		(
			float	*InImage,		/* input processing */
			float   *OutImg,      /* Output coefficient */
			int	Width,		/* width of the image */
			int	Height,		/* height of the image */
			int     SliceNum,     /* SliceNum of volumes */
			int     SplineDegree
		)

{ 

	float	*Line1, *Line2, *Line3;
	float	Pole[3];
	int	NbPoles;
	int	x, y;
	int     i,j,k,index;
       

        

	switch (SplineDegree) {

        case 1: 
			for(k=0;k<SliceNum*Height*Width;k++)
				OutImg[k]=InImage[k];

			return;
			break;

		case 2:
			NbPoles = 1;
			Pole[0] = (float)sqrt(8.0) - 3.0f;
			break;
		case 3:
			NbPoles = 1;
			Pole[0] = (float)sqrt(3.0) - 2.0f;
			break;
		case 4:
			NbPoles = 2;
			Pole[0] = (float)(sqrt(664.0 - sqrt(438976.0)) + sqrt(304.0) - 19.0);
			Pole[1] = (float)(sqrt(664.0 + sqrt(438976.0)) - sqrt(304.0) - 19.0);
			break;
		case 5:
			NbPoles = 2;
			Pole[0] = (float)(sqrt(135.0 / 2.0 - sqrt(17745.0 / 4.0)) + sqrt(105.0 / 4.0)
				- 13.0 / 2.0);
			Pole[1] = (float)(sqrt(135.0 / 2.0 + sqrt(17745.0 / 4.0)) - sqrt(105.0 / 4.0)
				- 13.0 / 2.0);
			break;

		case 7: 
			NbPoles = 3;
			Pole[0] =-0.53528f;
			Pole[1] =-0.122555f;
			Pole[2] =-0.00914869f;
			break;

		default:
			printf("Invalid spline degree\n");
			return ;
	}

	
			
	/* convert the image samples into interpolation coefficients */


        

	/* in-place separable process, along x */



	Line1 = (float *)calloc(Width,sizeof(float));
	if (Line1 == NULL) {
	  //printf("Row allocation failed\n");
		return;
	}
	Line2 = (float *)calloc(Height,sizeof(float));
	if (Line2 == NULL) {
	  //printf("Column allocation failed\n");
		return;
	}
	Line3 = (float *)calloc(SliceNum,sizeof(float));
	if (Line3 == NULL) {
	  //printf("Line along Slice direction  allocation failed\n");
		return;
	}


	for(k=0;k<SliceNum;k++)
	{	
         

		for (y = 0; y < Height; y++) {
                 
			index=k*Width*Height+y*Width;
			for(i=0;i<Width;i++) 
				Line1[i]=InImage[index+i];
			ConvertToInterpolationCoefficients(Line1, Width, Pole, NbPoles, CVC_DBL_EPSILON);
              
			for(i=0;i<Width;i++) 
				OutImg[index+i]=Line1[i];
		}
	

	/* in-place separable process, along y */
	
	
		for (x = 0; x < Width; x++) {
    
			index=k*Width*Height+x;
		
			for(j=0;j<Height;j++) 
				Line2[j]=OutImg[index+j*Width];
			ConvertToInterpolationCoefficients(Line2, Height, Pole, NbPoles, CVC_DBL_EPSILON);
			for(j=0;j<Height;j++) 
				OutImg[index+j*Width]=Line2[j];
		
		}
	

	} //end of k loop


     /* in-place separable process, along z */

	for(x=0;x<Width;x++)
		for(y=0;y<Height;y++)
		{

			index=y*Width+x;
			for(k=0;k<SliceNum;k++) 
				Line3[k]=OutImg[index+k*Width*Height];
			ConvertToInterpolationCoefficients(Line3, SliceNum, Pole, NbPoles, CVC_DBL_EPSILON);
			for(k=0;k<SliceNum;k++) 
				OutImg[index+k*Width*Height]=Line3[k];
		}
	free(Line1);
	free(Line2);
	free(Line3);
	return ;
} 

//------------------------





//-----------------------
//-----------------------
//----------------------




void    Resample(float *SplineCoeff, float *NewImg, int OldXDIM, int OldYDIM, int OldZDIM, int NewXDIM, int NewYDIM, int NewZDIM, int SplineDegree)
{



	int i,j,k;

	float posx,posy,posz,newvalue;
   
   

	int index;

   
	int NewXYDIM;


	NewXYDIM=NewXDIM*NewYDIM;


	if(SplineDegree==1)
	{	
		ExpandCoefficient(SplineCoeff, NewImg, OldXDIM,OldYDIM,OldZDIM,NewXDIM,NewYDIM,NewZDIM, SplineDegree);
		return;
	}

	for(k=0;k<NewZDIM;k++)
		for(j=0;j<NewYDIM;j++)
			for(i=0;i<NewXDIM;i++)
			{
				posx=i*(OldXDIM-1.0f)/(NewXDIM-1.0f);
				posy=j*(OldYDIM-1.0f)/(NewYDIM-1.0f);
                posz= NewZDIM > 1 ? k*(OldZDIM-1.0f)/(NewZDIM-1.0f): 0;

				newvalue=InterValue(SplineCoeff,OldXDIM,OldYDIM,OldZDIM,posx,posy,posz,SplineDegree);

				index=k*NewXYDIM+j*NewXDIM+i;

				NewImg[index]=newvalue;



			}


	return;

}




float InterValue(float *SplineCoeff,int OldXDIM,int OldYDIM, int OldZDIM,float posx,float posy,float posz, int SplineDegree)
{


       
	float	xWeight[6], yWeight[6],zWeight[6];
	float	interpolated;
	float	w, w2, w4, t, t0, t1, w1;
	int	xIndex[6], yIndex[6], zIndex[6];
	int	Width2 = 2*OldXDIM-2, Height2 = 2*OldYDIM-2, SliceNum2=2*OldZDIM-2;
	int	i, j, k, index;


	/* compute the interpolation indexes */
	if (SplineDegree & 1) {
		i = (int)floor(posx) - SplineDegree / 2;
		j = (int)floor(posy) - SplineDegree / 2;
		k = (int)floor(posz) - SplineDegree /2 ;

		for (index = 0; index <= SplineDegree; index++) {
			xIndex[index] = i++;
			yIndex[index] = j++;
			zIndex[index] = k++;
		}
	}
	else {
		i = (int)floor(posx + 0.5) - SplineDegree / 2;
		j = (int)floor(posy + 0.5) - SplineDegree / 2;
		k=  (int)floor(posz + 0.5) -  SplineDegree/2;
		
		for (index = 0; index <= SplineDegree; index++) {
			xIndex[index] = i++;
			yIndex[index] = j++;
			zIndex[index] = k++;
		}
	}



	/* compute the interpolation weights */
	switch (SplineDegree) {
		case 2:
			/* x */
			w =posx - (float)xIndex[1];
			xWeight[1] = 3.0f / 4.0f - w * w;
			xWeight[2] = (1.0f / 2.0f) * (w - xWeight[1] + 1.0f);
			xWeight[0] = 1.0f - xWeight[1] - xWeight[2];
			/* y */
			w = posy - (float)yIndex[1];
			yWeight[1] = 3.0f / 4.0f - w * w;
			yWeight[2] = (1.0f / 2.0f) * (w - yWeight[1] + 1.0f);
			yWeight[0] = 1.0f - yWeight[1] - yWeight[2];
                        /* z */
			w = posz - (float)zIndex[1];
			zWeight[1] = 3.0f / 4.0f - w * w;
			zWeight[2] = (1.0f / 2.0f) * (w - zWeight[1] + 1.0f);
			zWeight[0] = 1.0f - zWeight[1] - zWeight[2];
			break;
		case 3:
			/* x */
			w = posx - (float)xIndex[1];
			xWeight[3] = (1.0f / 6.0f) * w * w * w;
			xWeight[0] = (1.0f / 6.0f) + (1.0f / 2.0f) * w * (w - 1.0f) - xWeight[3];
			xWeight[2] = w + xWeight[0] - 2.0f * xWeight[3];
			xWeight[1] = 1.0f - xWeight[0] - xWeight[2] - xWeight[3];
			/* y */
			w = posy - (float)yIndex[1];
			yWeight[3] = (1.0f / 6.0f) * w * w * w;
			yWeight[0] = (1.0f / 6.0f) + (1.0f / 2.0f) * w * (w - 1.0f) - yWeight[3];
			yWeight[2] = w + yWeight[0] - 2.0f * yWeight[3];
			yWeight[1] = 1.0f - yWeight[0] - yWeight[2] - yWeight[3];
			/* z */
			w = posz - (float)zIndex[1];
			zWeight[3] = (1.0f / 6.0f) * w * w * w;
			zWeight[0] = (1.0f / 6.0f) + (1.0f / 2.0f) * w * (w - 1.0f) - zWeight[3];
			zWeight[2] = w + zWeight[0] - 2.0f * zWeight[3];
			zWeight[1] = 1.0f - zWeight[0] - zWeight[2] - zWeight[3];
			break;
		case 4:
			/* x */
			w = posx - (float)xIndex[2];
			w2 = w * w;
			t = (1.0f / 6.0f) * w2;
			xWeight[0] = 1.0f / 2.0f - w;
			xWeight[0] *= xWeight[0];
			xWeight[0] *= (1.0f / 24.0f) * xWeight[0];
			t0 = w * (t - 11.0f / 24.0f);
			t1 = 19.0f / 96.0f + w2 * (1.0f / 4.0f - t);
			xWeight[1] = t1 + t0;
			xWeight[3] = t1 - t0;
			xWeight[4] = xWeight[0] + t0 + (1.0f / 2.0f) * w;
			xWeight[2] = 1.0f - xWeight[0] - xWeight[1] - xWeight[3] - xWeight[4];
			/* y */
			w = posy - (float)yIndex[2];
			w2 = w * w;
			t = (1.0f / 6.0f) * w2;
			yWeight[0] = 1.0f / 2.0f - w;
			yWeight[0] *= yWeight[0];
			yWeight[0] *= (1.0f / 24.0f) * yWeight[0];
			t0 = w * (t - 11.0f / 24.0f);
			t1 = 19.0f / 96.0f + w2 * (1.0f / 4.0f - t);
			yWeight[1] = t1 + t0;
			yWeight[3] = t1 - t0;
			yWeight[4] = yWeight[0] + t0 + (1.0f / 2.0f) * w;
			yWeight[2] = 1.0f - yWeight[0] - yWeight[1] - yWeight[3] - yWeight[4];
			/* z */
			w = posz - (float)zIndex[2];
			w2 = w * w;
			t = (1.0f / 6.0f) * w2;
			zWeight[0] = 1.0f / 2.0f - w;
			zWeight[0] *= zWeight[0];
			zWeight[0] *= (1.0f / 24.0f) * zWeight[0];
			t0 = w * (t - 11.0f / 24.0f);
			t1 = 19.0f / 96.0f + w2 * (1.0f / 4.0f - t);
			zWeight[1] = t1 + t0;
			zWeight[3] = t1 - t0;
			zWeight[4] = zWeight[0] + t0 + (1.0f / 2.0f) * w;
			zWeight[2] = 1.0f - zWeight[0] - zWeight[1] - zWeight[3] - zWeight[4];
			break;
		case 5:
			/* x */
			w = posx - (float)xIndex[2];
			w2 = w * w;
			xWeight[5] = (1.0f / 120.0f) * w * w2 * w2;
			w2 -= w;
			w4 = w2 * w2;
			w -= 1.0f / 2.0f;
			t = w2 * (w2 - 3.0f);
			xWeight[0] = (1.0f / 24.0f) * (1.0f / 5.0f + w2 + w4) - xWeight[5];
			t0 = (1.0f / 24.0f) * (w2 * (w2 - 5.0f) + 46.0f / 5.0f);
			t1 = (-1.0f / 12.0f) * w * (t + 4.0f);
			xWeight[2] = t0 + t1;
			xWeight[3] = t0 - t1;
			t0 = (1.0f / 16.0f) * (9.0f / 5.0f - t);
			t1 = (1.0f / 24.0f) * w * (w4 - w2 - 5.0f);
			xWeight[1] = t0 + t1;
			xWeight[4] = t0 - t1;
			/* y */
			w = posy - (float)yIndex[2];
			w2 = w * w;
			yWeight[5] = (1.0f / 120.0f) * w * w2 * w2;
			w2 -= w;
			w4 = w2 * w2;
			w -= 1.0f / 2.0f;
			t = w2 * (w2 - 3.0f);
			yWeight[0] = (1.0f / 24.0f) * (1.0f / 5.0f + w2 + w4) - yWeight[5];
			t0 = (1.0f / 24.0f) * (w2 * (w2 - 5.0f) + 46.0f / 5.0f);
			t1 = (-1.0f / 12.0f) * w * (t + 4.0f);
			yWeight[2] = t0 + t1;
			yWeight[3] = t0 - t1;
			t0 = (1.0f / 16.0f) * (9.0f / 5.0f - t);
			t1 = (1.0f / 24.0f) * w * (w4 - w2 - 5.0f);
			yWeight[1] = t0 + t1;
			yWeight[4] = t0 - t1;
			/* z */
			w = posz - (float)zIndex[2];
			w2 = w * w;
			zWeight[5] = (1.0f / 120.0f) * w * w2 * w2;
			w2 -= w;
			w4 = w2 * w2;
			w -= 1.0f / 2.0f;
			t = w2 * (w2 - 3.0f);
			zWeight[0] = (1.0f / 24.0f) * (1.0f / 5.0f + w2 + w4) - zWeight[5];
			t0 = (1.0f / 24.0f) * (w2 * (w2 - 5.0f) + 46.0f / 5.0f);
			t1 = (-1.0f / 12.0f) * w * (t + 4.0f);
			zWeight[2] = t0 + t1;
			zWeight[3] = t0 - t1;
			t0 = (1.0f / 16.0f) * (9.0f / 5.0f - t);
			t1 = (1.0f / 24.0f) * w * (w4 - w2 - 5.0f);
			zWeight[1] = t0 + t1;
			zWeight[4] = t0 - t1;
			break;
		default:
			printf("Invalid spline degree\n");
			return(0.0);
	}

	/* apply the mirror boundary conditions */
	for (k = 0; k <= SplineDegree; k++) {
		xIndex[k] = (OldXDIM == 1) ? (0) : ((xIndex[k] < 0) ?
			(-xIndex[k] - Width2 * ((-xIndex[k]) / Width2))
			: (xIndex[k] - Width2 * (xIndex[k] / Width2)));
		if (OldXDIM <= xIndex[k]) {
			xIndex[k] = Width2 - xIndex[k];
		}
		yIndex[k] = (OldYDIM == 1) ? (0) : ((yIndex[k] < 0) ?
			(-yIndex[k] - Height2 * ((-yIndex[k]) / Height2))
			: (yIndex[k] - Height2 * (yIndex[k] / Height2)));
		if (OldYDIM <= yIndex[k]) {
			yIndex[k] = Height2 - yIndex[k];
		}
		zIndex[k] = (OldZDIM == 1) ? (0) : ((zIndex[k] < 0) ?
			(-zIndex[k] - SliceNum2 * ((-zIndex[k]) / SliceNum2))
			: (zIndex[k] - SliceNum2 * (zIndex[k] / SliceNum2)));
		if (OldZDIM <= zIndex[k]) {
			zIndex[k] = SliceNum2 - zIndex[k];
		}

               
	}

	/* perform interpolation */
	interpolated = 0.0;

	for(k=0;k<=SplineDegree;k++)
	{
		w1=0.0;
		for (j = 0; j <= SplineDegree; j++) {
			w = 0.0;
			for (i = 0; i <= SplineDegree; i++) {
				w += xWeight[i] * SplineCoeff[zIndex[k]*OldXDIM*OldYDIM+yIndex[j]*OldXDIM+xIndex[i]];
			}
			w1+= yWeight[j] * w;
		}
		interpolated += zWeight[k] * w1;
	}

	return(interpolated);
}
//---------------------------------------------------
//---------------------------------------------------
//----------------------------------------------------


//---------------------------------------------------
//----------------------------------------------------
//---------------------------------------

//-------------------------------------


void ExpandCoefficient(float *SplineCoeff, float *NewSplineCoeff, int NumVertX,int NumVertY, int NumVertZ, int NewNumVertX,int NewNumVertY, int NewNumVertZ, int ordernum)

{

  
	float realstepX,realstepY,realstepZ;

	int i,j,k;

	float realx,realy,realz;
  
	int floorofx,floorofy,floorofz,ceilofx,ceilofy,ceilofz;

	float tempvalue1,tempvalue2,tempvalue3,tempvalue4;

	float tempvalue5,tempvalue6,tempvalue7,tempvalue8;

	float alpha,finalvalue;  


  

  //---------------------

  

   
	realstepX =(NumVertX-1.0f)/(NewNumVertX-1.0f);
	realstepY =(NumVertY-1.0f)/(NewNumVertY-1.0f);
	realstepZ = NewNumVertZ > 1? (NumVertZ-1.0f)/(NewNumVertZ-1.0f): 0;


	for(k=0;k<NewNumVertZ;k++)
		for(j=0;j<NewNumVertY;j++) {
			for(i=0;i<NewNumVertX;i++) {
			{
				realx=i*realstepX;
				realy=j*realstepY;
				realz=k*realstepZ;

				floorofx=(int)floor(realx);
				floorofy=(int)floor(realy);
				floorofz=(int)floor(realz);
				if(floorofx<(NumVertX-1)) 
					ceilofx=floorofx+1; 
				else 
					ceilofx=floorofx;
				if(floorofy<(NumVertY-1)) 
					ceilofy=floorofy+1; 
				else 
					ceilofy=floorofy;
				if(floorofz<(NumVertZ-1)) 
					ceilofz=floorofz+1; 
				else 
					ceilofz=floorofz;
           
				tempvalue1=SplineCoeff[floorofz*NumVertY*NumVertX+floorofy*NumVertX+floorofx];
                               // (floorx,flooy,floorz) 
				tempvalue2=SplineCoeff[floorofz*NumVertY*NumVertX+floorofy*NumVertX+ceilofx];
                                // (ceilx,floory,floorz)
				tempvalue3=SplineCoeff[floorofz*NumVertY*NumVertX+ceilofy*NumVertX+floorofx];
                                //( floorx, ceily,floorz)
				tempvalue4=SplineCoeff[floorofz*NumVertY*NumVertX+ceilofy*NumVertX+ceilofx];
                                //ceilx,ceily,floorz);
 
           
          
				alpha=ceilofx-realx;
           
           
				tempvalue5=alpha*tempvalue1+(1.0f-alpha)*tempvalue2;
				tempvalue6=alpha*tempvalue3+(1.0f-alpha)*tempvalue4;
				alpha=ceilofy-realy;
				tempvalue7=alpha*tempvalue5+(1.0f-alpha)*tempvalue6;     // the value of (realx,realy,floorz)


				tempvalue1=SplineCoeff[ceilofz*NumVertY*NumVertX+floorofy*NumVertX+floorofx];
                               // (floorx,flooy,ceilz) 
				tempvalue2=SplineCoeff[ceilofz*NumVertY*NumVertX+floorofy*NumVertX+ceilofx];
                                // (ceilx,floory,ceilz)
				tempvalue3=SplineCoeff[ceilofz*NumVertY*NumVertX+ceilofy*NumVertX+floorofx];
                                //( floorx, ceily,ceilz)
				tempvalue4=SplineCoeff[ceilofz*NumVertY*NumVertX+ceilofy*NumVertX+ceilofx];
                                //ceilx,ceily,ceilz);
 
          
				alpha=ceilofx-realx;
				tempvalue5=alpha*tempvalue1+(1.0f-alpha)*tempvalue2;
				tempvalue6=alpha*tempvalue3+(1.0f-alpha)*tempvalue4;
				alpha=ceilofy-realy;
				tempvalue8=alpha*tempvalue5+(1.0f-alpha)*tempvalue6;     // the value of (realx,realy,ceilz)
           

				alpha=ceilofz-realz;
				finalvalue=alpha*tempvalue7+(1.0f-alpha)*tempvalue8;     // the final value of (realx,realy, realz);


				NewSplineCoeff[k*NewNumVertX*NewNumVertY+j*NewNumVertX+i]=finalvalue;


			}  
		}
	}// end of loop i,j,k



	return;

}  // end of routine Expand Coefficients



//--------------------------------
//----------------------------------------------------------







//-----------------------------------------------------------
//-----------------------------------------------------------
void MakePostFilter(int posz_start,int posz_end,float *Kernelz, int ordernum)
{

//	int i;

	switch(ordernum)
	{

		case 0:

			break;  
    
		case 1:

			break;

		case 2:

			break;

		case 3:

			break;

		case 4:

			break;

		case 5:

			break;

		default:

			break;



	} // end of loop switch


	return;

}

//----end of Making Post Filter

//------------------------------------

//------------------------------------



//----------------------------------------------
//---------------------------------------------

void PostFiltering(float *PostSplineCoeff, float *NewImg, int NewXDIM, int NewYDIM, int NewZDIM, int ordernum)
{

	int i,j,k;
	int index;
	int LengthFilter;

	float postfilterweight[3];


	postfilterweight[0]=1.0f/6.0f;
    postfilterweight[1]=2.0f/3.0f;
    postfilterweight[2]=1.0f/6.0f;


   

	switch(ordernum)
	{

   
		case 1:
        
			LengthFilter=3;
			break;

  
		case 3:

			LengthFilter=3;
			break;

  
		default:

			break;



	} // end of loop switch


   
	if(ordernum==1)
	{

		for(k=0;k<NewZDIM*NewYDIM*NewXDIM;k++) 
			NewImg[k]=PostSplineCoeff[k];
		return;

	}


    
    //--------------------------------//

     // the first pass


	for(k=0;k<NewZDIM;k++)
		for(j=0;j<NewYDIM;j++)
		{
			index=k*NewXDIM*NewYDIM+j*NewXDIM;
			NewImg[index]=PostSplineCoeff[index];
			NewImg[index+NewXDIM-1]=PostSplineCoeff[index+NewXDIM-1];
			for(i=1;i<NewXDIM-1;i++) 
			{
				index=k*NewXDIM*NewYDIM+j*NewXDIM+i;
				NewImg[index]=postfilterweight[0]*(PostSplineCoeff[index-1]+PostSplineCoeff[index+1])+postfilterweight[1]*PostSplineCoeff[index];

			}

		}
  
   
       
	for(k=0;k<NewZDIM;k++)
		for(j=0;j<NewYDIM;j++)
			for(i=0;i<NewXDIM;i++) 
			{
				index=k*NewXDIM*NewYDIM+j*NewXDIM+i;
				PostSplineCoeff[index]=NewImg[index];

			}
 

   // the second pass
   
	for(k=0;k<NewZDIM;k++)
		for(i=0;i<NewXDIM;i++)
		{
         
			index=k*NewXDIM*NewYDIM+i;
			NewImg[index]=PostSplineCoeff[index];
			NewImg[index+(NewYDIM-1)*NewXDIM]=PostSplineCoeff[index+(NewYDIM-1)*NewXDIM];
       
			for(j=1;j<NewYDIM-1;j++) 
			{
				index=k*NewXDIM*NewYDIM+j*NewXDIM+i;
				NewImg[index]=postfilterweight[0]*(PostSplineCoeff[index-NewXDIM]+PostSplineCoeff[index+NewXDIM])+postfilterweight[1]*PostSplineCoeff[index];

			}

		}
  
   
       
	for(k=0;i<NewZDIM;k++)
		for(j=0;j<NewYDIM;j++)
			for(i=0;i<NewXDIM;i++) 
			{
				index=k*NewXDIM*NewYDIM+j*NewXDIM+i;
				PostSplineCoeff[index]=NewImg[index];

			}
 



   // the third pass

 
	for(j=0;j<NewYDIM;j++)
		for(i=0;i<NewXDIM;i++) 
		{
			index=j*NewXDIM+i;
			NewImg[index]=PostSplineCoeff[index];
			NewImg[index+NewXDIM*NewYDIM*(NewZDIM-1)]=PostSplineCoeff[index+NewXDIM*NewYDIM*(NewZDIM-1)];
			for(k=1;k<NewZDIM-1;k++) 
			{
				index=k*NewXDIM*NewYDIM+j*NewXDIM+i;
				NewImg[index]=postfilterweight[0]*(PostSplineCoeff[index-NewXDIM*NewYDIM]+PostSplineCoeff[index+NewXDIM*NewYDIM])+postfilterweight[1]*PostSplineCoeff[index];

			}

		}
  
   
       
  
	return;

}
//------------------------------------------------
//-----------------------------------------------










