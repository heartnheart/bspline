#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>


void	BspineResample(char* Inputfile, char* Outputfile, int SplineOrder, int* startLocation, int* SubVolumeSize, int* newVolumeSize);
float	InitialAntiCausalCoefficient(float *, int, float);	
float	InitialCausalCoefficient(float *, int, float, float);
void	ConvertToInterpolationCoefficients(float *, int, float *, int ,float);


void    TransImg2Spline(float *, float *, int, int, int,int);

void    ExpandCoefficient(float *, float *,int, int, int, int, int, int, int);

void    Resample(float *, float *, int, int, int, int, int, int, int);
   

float InterValue(float *,int,int, int,float,float,float, int);			
void  MakePostFilter(int, int, float *, int);


 void PostFiltering(float *,float *,int,int,int,int);
