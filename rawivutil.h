/*************************************************************
This file provides an interface to manipulate rawiv files, as well as to
compute the root mean squared difference of two rawiv files, which are 
of the same dimensions and data type.
*************************************************************/
#include <stdio.h>
struct rawiv {
	float minext[3]; // coords of the first voxel
	float maxext[3]; // coords of the last voxel
	int nverts; // number of vertices = dim_x * dim_y * dim_z
	int ncells; // number of cells = (dim_x-1) * (dum_y-1) * (dim_z-1)
	unsigned int dim[3]; // number of vertices in x,y,z direction respectively
	float orig[3]; // redundant, should equal to minext
	float span[3]; // spacing between one vertex and the next: span_x = (max_x - min_x)/(dim_x - 1)
	unsigned char* ucDataset; 
	unsigned short* usDataset;
	float* fDataset;
};

void reverseAFloat(float* in);
void reverseAnInt(int* in);
void reverseAnUnInt(unsigned int* in);
void reverseAShort(short* in);
void reverseAnUnShort(unsigned short* in);

void rawivDataToFloat(struct rawiv& r);
void rawivDatatoUChar(struct rawiv& r);

void setRawivMinExt(struct rawiv& r, float[3]);
void setRawivMaxExt(struct rawiv& r, float[3]);
void setRawivDim(struct rawiv& r, int [3]);
void setRawivOrig(struct rawiv& r, float[3]);
void setRawivSpan(struct rawiv& r, float[3]);
void setRawivData(struct rawiv& r, unsigned char*);
void setRawivData(struct rawiv& r, unsigned short*);
void setRawivData(struct rawiv& r, float*);

void readRawivHeader(FILE* fp, float* minext, float* maxext, int& nverts, int& ncells, unsigned int* dim, float* orig, float* span);

void writeRawivHeader(FILE* fp, float* minext, float* maxext, int& nverts, int& ncells, unsigned int* dim, float* orig, float* span);

void readRawiv(char* filename, struct rawiv& r);


void writeRawiv(char* filename, struct rawiv& r); 


void freeRawiv(struct rawiv& r);



void readRawivFromImage(const char* filename, struct rawiv& r);
void writeRawivToImage(const char* filename, struct rawiv& r);